<div id="content">
 <p>
All-in-one desktop environments (DE) such as Gnome, KDE and those found in
popular systems get in the way of the power user in that they do not leave much
room for customization, it restricts the potential a computer can provide.
Moreover most popular DE have been plagued for decades with unpractical,
non-automated and limited window management.
</p>

 <p>
Positioning and resizing windows does not take much time  <i>per se</i>, but it stacks
up.  Think about how many windows you fire up every day (which, for some obscure
reasons, seem to be spawned at random places all the time on many popular
systems). Add up all the seconds you spend managing them, moving around,
resizing…  Wouldn’t it be nice if this were done for us?  After all, a computer
is an augmented  <i>automata</i>, it should be able to automate this.
</p>

 <p>
Another side-effect: the classic desktop environment (DE) will bundle more than
you want…  As such, they will always require more disk space and virtual memory
than you actually need.  The following advises will make things lighter and
faster.
</p>

 <div id="outline-container-orgc422dcf" class="outline-2">
 <h2 id="orgc422dcf">Dynamic Tiling Window Managers</h2>
 <div class="outline-text-2" id="text-orgc422dcf">
 <p>
There are ways to automate window management: tiling window managers.  Once we
have gotten used to them, it gets hard to bear with the waste of time incurred
by the “regular, user-friendly” window managers.  (Like those featured in Gnome
and KDE.) The latter are called stacking window managers.  I find the concept of
stacking fairly questionable: why would the user want to  <i>cover</i> part of other
windows? Either you want to have a full look at some of the currently opened
windows, or you just want to focus on one windows.  Which means either tile the
windows or go fullscreen.  In other words, unused screen space is wasted screen
space.  I know how badly we like to look at our wallpaper, but that is not the
primary use we should make of our computer.
</p>

 <p>
This leads us to the antagonists of the stacking window managers: the tiling
window managers.  Windows do not “float” anymore, i.e. their position and size is
not controlled manually but automatically so that it fits the screen space best.
Understand that tiling managers do no deprive you from manual control over the
windows: they are, by definition, a superset of stacking WMs, and thus you can
still resize and position windows as you see fit.
</p>

 <p>
The core idea is to be fast and display what the user wants.
</p>

 <p>
See how mobile devices work: the restricted screen space does not allow for
stacking windows, so all applications go fullscreen.
</p>

 <p>
TWMs allow the user to relay full window management to the WM independently of
the application capabilities.  For instance a good WM will let you go fullscreen
or use tabs with any program. This is part of the  <a href="http://en.wikipedia.org/wiki/Unix_philosophy">Unix philosophy</a>: instead of
duplicating functionalities across programs, write it once and for all where it
is meant to be.  File browsers and Internet browsers should not have tabs: WMs
will do a better job (grouping, sorting) while keeping consistent behaviour and
key bindings across programs.
</p>

 <p>
Last but not least, there is a subcategory among tiling WMs: the dynamic ones.
The tiling is done whenever a window is fired up.
</p>

 <p>
In theory, the concept of a window manager is rather simple.  In practice, they
mainly differ by their configuration.  Some WM will offer little tweaking
possibilities to the user, while other will allow programmable modifications.
The former is easier to get started with, while the latter will allow for the
exact desired behaviour.
</p>

 <p>
In my opinion, the most powerful WM would be a DTWM with a Turing-complete
configuration.  Such popular DTWMs include  <i>Awesome</i> and  <i>i3</i>. Awesome’s
configuration is in Lua, i.e. it allows virtually unlimited tweaking, while i3
relies on external shell scripts.  Another possibly more powerful example is
 <a href="../emacs-everywhere/index.html">EXWM</a> which is extensible in Emacs Lisp.
</p>
</div>
</div>

 <div id="outline-container-org7cf0f42" class="outline-2">
 <h2 id="org7cf0f42">Keyboard control</h2>
 <div class="outline-text-2" id="text-org7cf0f42">
 <p>
The use of the mouse has become so widespread with the advent of desktop
computers that it now seems “intuitive” to associate the use of a mouse together
with desktop environments.  All the desktop management (windows, firing up
programs, monitoring the system, etc.) are  <i>discrete actions</i>, they do not
require the continuous movement of a mouse pointer.  As such, everything can be
bound to a keybinding.  Go fullscreen?  Keybinding.  Change the window layout?
Keybinding.
</p>

 <p>
Keep in mind we say “keyboard shortcut”: the word  <i>shortcut</i> is there for a
reason.  See my article on  <a href="../keymap/index.html">mastering the keyboard</a>.
</p>
</div>
</div>

 <div id="outline-container-orga7143e4" class="outline-2">
 <h2 id="orga7143e4">Building from bricks</h2>
 <div class="outline-text-2" id="text-orga7143e4">
 <p>
Refraining from using a full-featured desktop environment means that you will
have to install the components one by one and configure it all to your liking.
Which is the only way to fully tailor a computer to your needs.  While it may
seem daunting at first, this is a one-time task: it is straightforward to re-use
the configuration across systems when it is stored in text files.
</p>
</div>

 <div id="outline-container-org45a6e4b" class="outline-3">
 <h3 id="org45a6e4b">Text configuration</h3>
 <div class="outline-text-3" id="text-org45a6e4b">
 <p>
Most desktop environments come with a configuration panel: a program that is
meant to centralize configuration.  In practice, this is an additional layer of
complexity that is not really needed.
</p>

 <p>
Editing a text file has many advantages:
</p>

 <ul class="org-ul"> <li>It is just as fast.  Even faster with a good text editor.</li>
 <li>A GUI may not be exhaustive: a text file guarantees full access to the
options.</li>
 <li>It is more reliable since it removes the extra layer of complexity.  (The GUI
might introduce bugs.)</li>
 <li>You keep control over configuration file updates.</li>
 <li>The configuration can be saved and even put under version control.</li>
</ul> <p>
This last point is key: it allows you to watch over the evolution of your
configuration changes (diffs); it also lets you deploy and sync a configuration
across several computers in an instant.  Last but not least, it lets you share
the configuration!
</p>
</div>
</div>

 <div id="outline-container-org7fd1f00" class="outline-3">
 <h3 id="org7fd1f00">Independent power-apps</h3>
 <div class="outline-text-3" id="text-org7fd1f00">
 <p>
Desktop environments push for developing “desktop-specific” applications, such
as the K* programs for KDE vs. the G* programs for Gnome.  I find this very
embarrassing: why would we spend months and years developing a program for a
sub-group of people?  Why not spreading it to the world?  Is it a form of
geek-racism?
</p>

 <p>
Our favorite programs should not have to do with the desktop environment.  If the
latter goes out of fashion and gets unmaintained, does it mean we are supposed
to change the whole set of our programs, possibly losing some functionalities in
the process?
</p>

 <p>
A “desktop” in my sense is just the WM with a launcher.  The rest should be the
user favorite programs: a terminal emulator, a desktop monitor, a text editor,
etc.  Every feature from a desktop environment is a program.  Missing this volume
icon?  Add key bindings.  Want to tweak the network configuration?  Fire-up the
appropriate tool with a keybinding, or go edit the text file full of endless
possibilities…
</p>

 <p>
See my  <a href="../power-apps/index.html">list of power-user applications</a> for some
inspiration.
</p>
</div>
</div>
</div>

 <div id="outline-container-org0f33d81" class="outline-2">
 <h2 id="org0f33d81">Black & White</h2>
 <div class="outline-text-2" id="text-org0f33d81">
 <p>
When the mere mortals have a glimpse at my computer, I would expect their first
reaction to be “What system is it?” or “Why is it so barren?”.  No, it is
invariably “Maaaah, why is it all black?!?”
</p>

 <p>
I find it interesting that black has become shocking.  Black used to be the
standard back in the days.  The white-dominant background may have been
popularized by the “user-friendliness” marketing of Apple and Microsoft in the
1980s as a reaction against the “hard-to-use” Unix and friends: the color was
here to mark the difference.  The association of ideas was easily made: white is
brightness, it is light, as opposed to the uncomfortable, insecure darkness of
Mr. Black.
</p>

 <p>
Now if we are rational, it is just a color.  But there is more to it.  Working on
a dark background will not strain the eye as much.  The whole industry is warning
its professional, intensive users against the possible eye strains and headaches
resulting from staring at a screen for too many hours.
</p>

 <p>
Solutions are developed: take a break, use eye drops, and whatnot.  While it
would be so much easier to eliminate the problem in the first place…  Listen to
the wisdom of our forefathers, they were bright people!  Working on a dark
background will solve the problems all at once. To the point that once you get
used to it, switching to a bright background will stun you on the spot!
</p>
</div>
</div>
</div>