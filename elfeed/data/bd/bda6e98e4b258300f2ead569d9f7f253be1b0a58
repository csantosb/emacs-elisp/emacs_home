<p><span style="font-weight: 400;"><div class="ttt-drop-cap"></span></p>
<p>Dicen que en el infinito cabe todo. Sencillamente, al no haber término en él, se halla todo lo posible… salvo un refugio, como alude <strong>el título del libro de Antonino Nieto Rodríguez. <em>En el infinito no hay refugio</em> (2020)</strong> se presenta en la colección Anaquel de Poesía de la editorial Cuadernos del Laberinto, pero, en verdad, hallamos una poesía que parece que deja de serlo y se transmuta en prosa, en aforismo, en reflexión, en exhortación, en crítica… En lo finito de esta obra hay infinidades de poemas, por ejemplo, sobre esto mismo:</p>
<p><span style="font-weight: 400;"></div></span></p>
<p>ondas en el infinito océano de lo sin fin</p>
<p>anclas de la nada en el barrizal de borrados, guiños,</p>
<p>cuentas…</p>
<p>letras mudas en el ciego carrusel de asaltos contra</p>
<p>la sordera infame</p>
<p>de todo esto nada sabes</p>
<p>y para desguace del aire que te acuna inmenso</p>
<p>imperdible, inútil,</p>
<p>lo llevas inscrito en el ventanal de tus ojos</p>
<p>Uno de los rasgos mayormente comunes de los poemas es la brevedad. Antonino Nieto consigue tal intensidad en algunas ocasiones que con<strong> uno, dos o tres versos expresa mucho más la sugerencia</strong> o la imagen que van cimentando la variedad temática:</p>
<p>recuerda</p>
<p>el miedo, el amigo más fiel</p>
<p>el que has de perder</p>
<p>vivir sí importa: te quiero</p>
<p>&nbsp;</p>
<p>estado?</p>
<p>esfafa!</p>
<p>Valga este último texto para dar pie a uno de los aspectos más llamativos del libro: el de la crítica política-moral, pues en estos tiempos inciertos en los que cada palabra ha de ser medida y cualquier discurso corre el peligro de ser acosado y censurado por bien pensantes que exigen un único mensaje, lo que está en contra de la libertad, <strong>Antonino Nieto no renuncia a esta en ningún momento</strong> y tiene claro que tanto la izquierda como la derecha representan su papel en estas sórdidas situaciones:</p>
<p>la izquierda encadena</p>
<p>la derecha exprime</p>
<p>y así,</p>
<p>entre celda y licuado,</p>
<p>el extinto ciudadano,</p>
<p>la culpa,</p>
<p>nada suma,</p>
<p>salvo el obligado incumplimiento del valor supremo:</p>
<p>[…]</p>
<p>la izquierda</p>
<p>inocula en el inocente o sentenciado</p>
<p>–la misma maldita gracia–</p>
<p>la infecta semilla de la expropiación de mentes</p>
<p>y haciendas</p>
<p>la derecha se la salta</p>
<p>y con ella hace caja</p>
<p><a href="https://cdn.zendalibros.com/wp-content/uploads/antonino_nieto2.jpg"><img class="aligncenter size-full wp-image-151529" src="https://cdn.zendalibros.com/wp-content/uploads/antonino_nieto2.jpg" alt="" width="888" height="1184" srcset="https://cdn.zendalibros.com/wp-content/uploads/antonino_nieto2.jpg 888w, https://cdn.zendalibros.com/wp-content/uploads/antonino_nieto2-225x300.jpg 225w, https://cdn.zendalibros.com/wp-content/uploads/antonino_nieto2-768x1024.jpg 768w" sizes="(max-width: 888px) 100vw, 888px" /></a></p>
<p>Desde el poder se quiere más poder, el dinero es el que lo marca, no hay signo político que no se beneficie de ello para mantener su dominio, lo que llega también a la cultura, de lo que se impregna, además, a la realidad que <strong>«como la solidaridad, la justicia, la libertad y demás conceptos científicamente alambrados: tubo de escape para el mantenimiento del status quo».</strong></p>
<p>El mismo poeta construye textos sobre la palabra, la poesía, la literatura… ante las polémicas y alude a la autocensura:</p>
<p>y dices que me autocensure,</p>
<p>que no diga</p>
<p>que cuidado con el islam…</p>
<p>Y es que todo se relaciona con el binomio estado-religión. Antonino Nieto lo escribe magníficamente de este modo:</p>
<p>las religiones una máscara para mantenernos</p>
<p>al servicio del bozal?</p>
<p>&nbsp;</p>
<p>lo laico, retirada de la máscara, el bozal sin más?</p>
<p>hablo del capital: del estado</p>
<p><em>En el infinito no hay refugio</em> consta de <strong>seis partes diversas, complementarias entre sí</strong>. La primera, «pulsos de sangre en el retal del barro: las rosas», es la más extensa, con una amplia variedad de temas; «la cloaca fermentada» es el título de la segunda parte, con una crítica a un Estado controlador con la corrección política («la nueva iglesia, el nuevo dios laico: el estado»); «el laberinto, la lluvia, el margen: el grito de los muertos» se denomina la tercera, con atención a las sensaciones, como la soledad, la felicidad, el abatimiento…; la cuarta, «contra nosotros mismos», establece los mandamientos de esa religión llamada corrección política; la quinta, «a modo de incensario: cronología del hambre», es una de las más interesantes, entre la poesía, el recuerdo, la sugerencia y la reflexión sobre hechos contemporáneos; finalmente, «los datos: en el infinito no hay paraíso» resulta un canto provocativo para poner de manifiesto el paso atrás de la razón, el paso adelante a la deshumanización, en la sociedad actual (parte del suceso del autobús que afirmaba que los niños tienen pene y las niñas, vulva; y continúa con menciones a la censura de los cuentos de Pipi Calzaslargas en Suecia o el niño que dejaron morir en EE.UU. por no permitir a los padres un tratamiento experimental a su enfermedad). Parece claro que ni en el infinito hallaremos refugio ante lo que sacude la libre expresión, tal y como Antonino Nieto Rodríguez comienza este poemario:</p>
<p>ni piedad</p>
<p>ni eternidad</p>
<p>en el infinito no hay refugio</p>
<p>Antonino Nieto Rodríguez es <strong>poeta, videoartista, creador de espectáculos y rituales en los que aúna literatura, artes plásticas, circo, ballets&#8230;</strong> Sus obras se han visto en el MNCARS, el IVAM, en festivales de Alemania y Portugal, en diversas Universidades, foros culturales, castillos, cementerios, antiguos campos de concentración, plazas públicas, bares…</p>
<p>Es colaborador habitual en diferentes programas de radio y revistas culturales; y además forma parte del equipo de Ámbito Cultural de El Corte Inglés.</p>
<p>Ha publicado, entre otros, los siguientes poemarios: <em>Dibujas ausentes, La voz del escorpión, Un fantasma perfecto, Toda la carne y el infinito</em>, <em>Escaleras del aire, El ojo del abismo toma de la mano el arco iris </em>y<em> En el infinito no hay refugio.</em></p>
<p><span style="font-weight: 400;">—————————————</span></p>
<p><b>Autor</b><span style="font-weight: 400;">: Antonino Nieto Rodríguez. </span><b>Título</b><span style="font-weight: 400;">: <em>En el infinito no hay refugio</em>. </span><b>Editorial</b><span style="font-weight: 400;">: Cuadernos del laberinto. </span><b>Venta</b><span style="font-weight: 400;">: <a href="https://www.todostuslibros.com/libros/en-el-infinito-no-hay-refugio_978-84-121309-9-7">Todostuslibros</a> y <a href="https://amzn.to/3k0LdFb">Amazon </a></span></p>
<p>La entrada <a rel="nofollow" href="https://www.zendalibros.com/en-el-infinito-no-hay-refugio-de-antonino-nieto-rodriguez/">En el infinito no hay refugio, de Antonino Nieto Rodríguez</a> aparece primero en <a rel="nofollow" href="https://www.zendalibros.com">Zenda</a>.</p>
