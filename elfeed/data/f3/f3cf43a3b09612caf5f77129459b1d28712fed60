<ul class="org-ul">
<li><a href="https://emacsconf.org/2021/cfp/">EmacsConf 2021 Call for Proposals</a> (<a href="https://reddit.com/r/emacs/comments/oyl40m/emacsconf_2021_call_for_proposals/">Reddit</a>, <a href="https://news.ycombinator.com/item?id=28075442">HN</a>) until Sept 30</li>
<li>Upcoming events:
<ul class="org-ul">
<li>EmacsConf Office Hour <a href="https://emacsconf.org/2021/office-hours/">https://emacsconf.org/2021/office-hours/</a> Tue Sep 21 1800 Vancouver / 2000 Chicago / 2100 Toronto &#x2013; Wed Sep 22 0100 GMT / 0300 Berlin / 0630 Kolkata / 0900 Singapore</li>
<li>Emacs APAC (virtual, in English) <a href="https://emacs-apac.gitlab.io/">https://emacs-apac.gitlab.io/</a> Sat Sep 25 0130 Vancouver / 0330 Chicago / 0430 Toronto / 0830 GMT / 1030 Berlin / 1400 Kolkata / 1630 Singapore</li>
<li>EmacsConf Office Hour <a href="https://emacsconf.org/2021/office-hours/">https://emacsconf.org/2021/office-hours/</a> Tue Sep 28 1800 Vancouver / 2000 Chicago / 2100 Toronto &#x2013; Wed Sep 29 0100 GMT / 0300 Berlin / 0630 Kolkata / 0900 Singapore</li>
<li>Emacs Berlin (virtual, in English) <a href="https://emacs-berlin.org/">https://emacs-berlin.org/</a> Wed Sep 29 0930 Vancouver / 1130 Chicago / 1230 Toronto / 1630 GMT / 1830 Berlin / 2200 Kolkata &#x2013; Thu Sep 30 0030 Singapore</li>
<li>M-x Research (contact them for password): TBA <a href="https://m-x-research.github.io/">https://m-x-research.github.io/</a> Fri Oct 1 0700 Vancouver / 0900 Chicago / 1000 Toronto / 1400 GMT / 1600 Berlin / 1930 Kolkata / 2200 Singapore</li>
</ul></li>
<li>Emacs configuration:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/pq6cjy/bikeshedding_friday_how_do_you_organize_your_init/">Bikeshedding Friday: How do you organize your init file?</a></li>
<li><a href="https://www.youtube.com/watch?v=knRI1O_XSeY">My Current Emacs Configuration</a> (34:34)</li>
<li><a href="https://svn.red-bean.com/repos/kfogel/trunk/.emacs">Since 1992, an 8,000 lines long, old-style .emacs, still currently updated [Karl Fogel]​</a> (<a href="https://www.reddit.com/r/emacs/comments/pqqs69/since_1992_an_8000_lines_long_oldstyle_emacs/">Reddit</a>)</li>
<li><a href="https://github.com/chriztheanvill/emacs">chriztheanvill's config for C++, Markdown, Org and later for lua</a></li>
<li><a href="https://github.com/pascalfleury/emacs-config">pascalfleury's personal Emacs configs, consisting mostly of configuration of free tools.</a></li>
</ul></li>
<li>Emacs Lisp:
<ul class="org-ul">
<li><a href="https://github.com/alphapapa/taxy.el/tree/master/examples#deffy">deffy.el: Show definitions and top-level forms in an Elisp project or file</a> (<a href="https://www.reddit.com/r/emacs/comments/poufna/deffyel_show_definitions_and_toplevel_forms_in_an/">Reddit</a>)</li>
<li><a href="https://emacspeak.blogspot.com/2021/09/snarfing-string-within-delimiters-with.html">T. V. Raman: Snarfing String Within Delimiters  With One Defun</a></li>
<li><a href="https://emacspeak.blogspot.com/2021/09/generalize-snarf-tool-how-general-can.html">T. V. Raman: Generalize Snarf Tool: How The General Can Be Simpler Than The Specific</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/pon0ee/transient_api_example_part_2_transientdostay/">Transient API Example (Part 2: transient-do-stay)</a></li>
<li><a href="https://gitlab.com/jpellegrini/eltr">eltr : ELTR is an Emacs Lisp Terminal REPL.</a></li>
<li><a href="http://ag91.github.io/blog/2021/09/19/moldable-emacs-making-molds-a-little-easier-to-write">Andrea: Moldable Emacs: making molds a little easier to write</a></li>
</ul></li>
<li>Emacs development:
<ul class="org-ul">
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=644d0ba589b94b1a23702cf8bad86b70204196f8">Add support for url-retrieve-synchronously to eww-retrieve-command (bug#50680)</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=775fc5312b8a5775e2089532f757c081d0fb9a80">* etc/NEWS: Add section on recent checkdoc changes.</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=bc59c98f096f7d01cbccf98d4fdd9c3f0385e896">Add new '/opme' and '/deopme' convenience ERC commands</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=c53aff5de71d7c558c222c35fd1cda0298d834b1">Add new help-enable-symbol-autoload user option</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=32d85f7b89e21986bcfc7b9caa67ed2dc39f886a">Mention xref-quit-and-pop-marker-stack in the manual</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=bd6dca038e9dfc4ad5b0bc07fcc16244e7073b28">Do some xwidget NEWS markup (and adjust doc string)</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=4e21c5f451a18f96172e63dbe8a3ceef780758bb">Check for null bytes in filenames in 'expand-file-name' (bug#49723)</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=931a898776065bf8f286c4d6491aebb782863c99">* etc/NEWS: Announce update of IRC-related references to point to Libera.Chat.</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=a8103cf9e08cdc159c4824d2b3b08cc5882fae76">Stop imenu indexing after a certain number of seconds</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=12d2fb58c416b557924174f57bfb1c9b9e7cf999">Split Unicode emoji into their own script</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=0cf0a2b98671671bb7639a17639ef2552b772cbe">Add new sequence function 'seq-union'</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=c6eb114a42922af18818dce7317238e0af776958">ERC: NickServ: Prompt for password last, overall simplifications (bug#46777)</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=7f53446a10eaf029f73e637bba3c9c6bf7c33111">Doc fix for y-or-n-p; trailing space is no longer needed</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=7a69fe3bc993f2599dacf653b43e4cba72456ac1">Merge branch 'feature/rcirc-update'</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=83557511a7e06d40740d3bf7acbb224eb3a8f150">Update Unicode support to Unicode version 14.0.0</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=fbef1ee018d9229be0c7d9af9eaa2774d430149c">Mention `lisp-data-mode' in `emacs-lisp-mode' doc string</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=8eb9eb0c41417991432122795522f6db7e1bb7d2">Allow for multiple attempts when reconnecting</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=159dbd5eb211e36d98e200781f2eae93f0973aeb">Make `find-function-source-path' into obsolete alias</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=4633e02726fbd683332eb64dee97b109aef4361f">Add support for project-wide diagnostics in Flymake (bug#50244)</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=eeb6d9d221d99f65c7995d065f9e11b2ef102c6b">Project File Commands manual clarification</a></li>
<li><a href="http://git.savannah.gnu.org/cgit/emacs.git/commit/etc/NEWS?id=cf2fa6c87f4da4665ff8a9e8e220bba0b5bccefc">Add user option to avoid checkdoc warning for unescaped left paren</a></li>
</ul></li>
<li>Appearance:
<ul class="org-ul">
<li><a href="https://www.youtube.com/watch?v=n920BQM_ic4">Emacs Beautification Using Mode-Line Dashboard And Themes</a> (12:15)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/prg8k6/screenshot_sunday_what_does_your_emacs_look_like/">Screenshot Sunday: What does your Emacs look like today?</a></li>
</ul></li>
<li>Navigation:
<ul class="org-ul">
<li><a href="https://www.youtube.com/watch?v=7eWAfmABHMs">Emacs: introduction to bookmarks</a> (24:21)</li>
</ul></li>
<li>Org Mode:
<ul class="org-ul">
<li><a href="https://www.youtube.com/watch?v=NZZVRCo1dLc">Emacs Org-Mode Intro</a> (13:15)</li>
<li><a href="https://imbmax.com/posts/emacs-as-a-time-tracker/">Emacs as a Time Tracker</a> (<a href="https://www.reddit.com/r/planetemacs/comments/ppcbsh/emacs_as_a_time_tracker/">Reddit</a>)</li>
<li><a href="https://youtu.be/TfOaaq5ibYQ">Simple Gantt chart from an Org todo list with SVG lib</a> (<a href="https://www.reddit.com/r/emacs/comments/prezj6/simple_gantt_chart_from_an_org_todo_list_with_svg/">Reddit</a>)</li>
<li><a href="https://www.reddit.com/r/emacs/comments/prb4vc/weblorg_static_html_generator_new_release_012/">Weblorg Static HTML Generator: New Release 0.1.2</a></li>
<li><a href="https://lists.gnu.org/archive/html/emacs-orgmode/2021-09/msg00179.html">[feature proposal] Headline caching via org-element-cache = up to 2.5x p</a></li>
<li>Org Roam:
<ul class="org-ul">
<li><a href="https://www.eigenbahn.com/2021/09/15/org-roam">Eigenbahn: Graphing my External Brain with Org-Roam</a></li>
<li><a href="https://youtu.be/CUkuyW6hr18">5 Org Roam Hacks for Better Productivity in Emacs</a> (<a href="https://www.reddit.com/r/emacs/comments/pnlwgb/5_org_roam_hacks_for_better_productivity_in_emacs/">Reddit</a>)</li>
</ul></li>
<li><a href="http://xenodium.com/plain-org-for-ios-a-month-later">Alvaro Ramirez: Plain Org for iOS (a month later)</a></li>
<li><a href="https://v.redd.it/m26q61kyggo71">Plain Org for iOS: latest render + inline edit experience</a> (<a href="https://www.reddit.com/r/emacs/comments/pr7gl5/plain_org_for_ios_latest_render_inline_edit/">Reddit</a>)</li>
</ul></li>
<li>Completion:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/ppg98f/which_completion_framework_do_you_use_and_why/">Which completion framework do you use and why?</a></li>
<li><a href="https://github.com/larkery/ido-grid.el">ido-grid.el: reimplementation of ido-grid-mode to be simpler/faster</a></li>
</ul></li>
<li>Coding:
<ul class="org-ul">
<li><a href="https://github.com/Artawower/turbo-log">turbo-log: Fast log message inserting for quick debug.</a></li>
<li><a href="https://francopasut.netlify.app/post/emacs_expand_region/">Emacs, Expand-region and LaTeX</a> (<a href="https://irreal.org/blog/?p=9973">Irreal</a>)</li>
<li><a href="https://oylenshpeegul.gitlab.io/blog/posts/20210917/">Tim Heaney: Haskell</a></li>
<li><a href="https://www.reddit.com/r/emacs/comments/pqekp7/emacs_configuration_for_rails_w_doom/">Emacs Configuration for Rails (w/ doom)</a></li>
<li><a href="https://youtu.be/giPPZWrH59g">Xah Html Mode got refactored</a> (<a href="https://www.reddit.com/r/emacs/comments/ppittz/xah_html_mode_got_refactored/">Reddit</a>)</li>
</ul></li>
<li>Shells:
<ul class="org-ul">
<li><a href="https://github.com/amno1/emacs-term-toggle">Term-toggle: Quake-style popup console for Emacs just got refactored</a> (<a href="https://www.reddit.com/r/emacs/comments/ppnk0u/termtoggle_quakestyle_popup_console_for_emacs/">Reddit</a>)</li>
</ul></li>
<li>Community:
<ul class="org-ul">
<li><a href="https://www.reddit.com/r/emacs/comments/polxft/weekly_tips_tricks_c_thread/">Weekly Tips, Tricks, &amp;c. Thread</a></li>
</ul></li>
<li>Other:
<ul class="org-ul">
<li><a href="https://kfx.fr/e/koe-PIM-crypt.el">koe-PIM-crypt.el: KOE - Simple gpg setup for org-crypt/EXWM</a> (2020)</li>
<li><a href="https://sheer.tj/the_way_of_emacs.html">The Way of Emacs</a> (2020, <a href="https://www.reddit.com/r/emacs/comments/pnoue0/the_way_of_emacs/">Reddit</a>)</li>
<li><a href="https://www.youtube.com/watch?v=PE714pm1kvE">System Crafters Live! - The Many Varieties of Emacs</a> (48:27)</li>
<li><a href="https://www.youtube.com/watch?v=JMP8JjmS3ds">System Crafters Live! - The Many Varieties of Emacs (Part 2)</a> (01:07:42)</li>
</ul></li>
<li>New packages:
<ul class="org-ul">
<li><a target="_blank" href="http://melpa.org/#/justl">justl</a>: Major mode for driving just files</li>
<li><a target="_blank" href="http://melpa.org/#/khalel">khalel</a>: Import, edit and create calendar events throuh khal</li>
<li><a target="_blank" href="http://melpa.org/#/newspeak-mode">newspeak-mode</a>: Major mode for the Newspeak programming language</li>
<li><a target="_blank" href="http://melpa.org/#/qrencode">qrencode</a>: QRCode encoder</li>
<li><a target="_blank" href="http://melpa.org/#/ue">ue</a>: Minor mode for Unreal Engine projects</li>
</ul></li>
</ul>

<p>
Links from <a href="https://www.reddit.com/r/emacs">reddit.com/r/emacs</a>, <a href="https://www.reddit.com/r/orgmode">r/orgmode</a>, <a href="https://www.reddit.com/r/spacemacs">r/spacemacs</a>, <a href="https://www.reddit.com/r/planetemacs">r/planetemacs</a>, <a href="https://hn.algolia.com/?query=emacs&amp;sort=byDate&amp;prefix&amp;page=0&amp;dateRange=all&amp;type=story">Hacker News</a>, <a href="https://planet.emacslife.com">planet.emacslife.com</a>, <a href="https://www.youtube.com/playlist?list=PL4th0AZixyREOtvxDpdxC9oMuX7Ar7Sdt">YouTube</a>, <a href="http://git.savannah.gnu.org/cgit/emacs.git/log/etc/NEWS">the Emacs NEWS file</a>, <a href="https://emacslife.com/calendar/">Emacs Calendar</a> and <a href="http://lists.gnu.org/archive/html/emacs-devel/2021-09">emacs-devel</a>.
</p>
