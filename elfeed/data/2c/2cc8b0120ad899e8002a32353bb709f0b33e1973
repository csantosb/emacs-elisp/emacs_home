<p>
One of Emacs power features that I&#39;ve never quite gotten into is
<a href="https://www.gnu.org/software/emacs/manual/html_node/emacs/Dired.html">dired</a>, the Directory Editor. I&#39;ve already done a <a href="https://cestlaz.github.io/posts/using-emacs-38-dired/">video on the package</a>.
It&#39;s really cool and I do use it at times but I still haven&#39;t started
using it for my day to day.</p>
<p>
Well, I just started using a couple of packages that might change
this. They&#39;re part of a set of dired addons that you can check out at the
<a href="https://github.com/Fuco1/dired-hacks/tree/2c1234592aee91dcd9401bcd67213e6a4a464fd9">dired hacks</a> page. The specific ones I&#39;m talkign about are
<code>dired-subtree</code> and <code>dired-narrow</code>. <code>dired-subtree</code> is cool but it&#39;s
not the game changer. <code>dired-narrow</code> is. One of my issues with
dired is that I can&#39;t see all the files I&#39;m operating on easily at the
same time. If I&#39;m marking a large number of files to delete I want to
be able to see all of them at once so I&#39;m sure I&#39;m not making a
mistake. <code>dired-narrow</code> lets me easily do this.</p>
<p>
Check out the video to see the details:</p>
  <iframe width="560" height="315" src="https://www.youtube.com/embed/pZzDayi5lRo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
