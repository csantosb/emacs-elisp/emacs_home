<p>Timo Geusch has a nice quickie post on <a href="https://www.lonecpluspluscoder.com/2021/09/07/emacs-enable-multiple-minor-modes-from-major-mode/">setting multiple minor modes for a buffer</a>. Often it’s convenient to have several minor modes associated with a buffer. For example, when I’m writing blog or prose text I like to have</p>
<ol class="org-ol">
<li><code>visual-line-mode</code></li>
<li><code>wc-mode</code></li>
<li><code>flyspell-mode</code></li>
</ol>
<p>turned on and <code>auto-fill-mode</code> turned of. That’s enough modes that it’s inconvenient to set them by hand. The normal solution is to turn them on in the local variables list at the top or bottom of the file.</p>
<pre class="example" id="orga5c742a"># -*- eval: (visual-line-mode 1); eval: (auto-fill-mode -1); eval: (wc-mode 1); eval: (flyspell-mode 1;) -*-
</pre>
<p>That works well and is what I do for non-blog writing but you have to take some sort of action to get it into each buffer such as including it or copying it by hand.</p>
<p>Geusch has another solution. He wrote a simple function to turn on the minor modes he needs and then runs that function in a hook for the type of buffer he’s interested in. That’s really easy if you use the <code>use-package</code> macro to configure your packages because you can use its <code>:hook</code> keyword to cause the function to be called. See Geusch’s post for the details and sample code.</p>
<p>It’s a simple idea and easy to implement but it can make your workflow a bit easier. Definitely worth taking a look at.</p>
